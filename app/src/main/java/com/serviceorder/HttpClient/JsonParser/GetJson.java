package com.serviceorder.HttpClient.JsonParser;

import android.app.Activity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.serviceorder.MainFragment;
import com.serviceorder.NavigationActivity;

import org.json.JSONArray;

/**
 * Created by Tomasz on 01.07.2017.
 */

public class GetJson {

    JsonArrayRequest jsonArrayRequest;
    private String tempJson;

    public void jsonFromServerRequest (final MainFragment.VolleyCallback callback, String requestUrl, RequestQueue queue) {

        jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try{
                    callback.onSuccess(response);
                    System.out.println("Response: " + response.toString());

                } catch (Exception e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());

            }
        });
        queue.add(jsonArrayRequest);

    }


    public String useData(final RequestQueue queue, final Activity activity) {
        jsonFromServerRequest(new MainFragment.VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                tempJson = result.toString();
                System.out.println("tempJson z GetJson " + tempJson );
                if(activity instanceof NavigationActivity) {
//                    MainFragment fragment = new MainFragment();
//                    fragment.runAsync();
                }
            }
        }, "http://192.168.0.19:8050/getAll", queue);
        return tempJson;
    }

}

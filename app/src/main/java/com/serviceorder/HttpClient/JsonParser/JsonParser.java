package com.serviceorder.HttpClient.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import com.serviceorder.Model._Model;

/**
 * Created by Tomasz on 22.06.2017.
 */

public class JsonParser {


    public JSONObject toJson(_Model model) {

        JSONObject jsonObject = new JSONObject();

        JSONArray array = new JSONArray();
        List<?> list;
        List<JSONObject> listOfJson = new ArrayList<>();
        for (Field field : model.getClass().getDeclaredFields()) {

            try {
                String fieldName = field.getName();
                field.setAccessible(true);
                Object fieldValue = field.get(model);
                if (fieldValue != null) {

                    if (field.getType().equals(List.class)) {
                        list = (List<?>) field.get(model);


                        for(int j=0; j<list.size(); j++){
                            JSONObject newJsonObject = new JSONObject();

                        for (Field newField : list.get(j).getClass().getDeclaredFields()) {

                            String newFieldName = newField.getName();
                            newField.setAccessible(true);

                            Object newFieldValue = newField.get(list.get(j));
                            if (newFieldValue != null) {
                                try {
                                    newJsonObject.put(newFieldName, newFieldValue);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                            listOfJson.add(newJsonObject);
                        }
                        try {
                            jsonObject.put(fieldName, new JSONArray(listOfJson));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                        else {
                        try {
                            jsonObject.put(fieldName, fieldValue);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception e) {
            }

        }
        return jsonObject;
    }


}

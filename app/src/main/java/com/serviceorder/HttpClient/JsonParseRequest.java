package com.serviceorder.HttpClient;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.serviceorder.HttpClient.JsonParser.JsonParser;
import com.serviceorder.MainFragment;
import com.serviceorder.Model._Model;
import com.serviceorder.NavigationActivity;
import com.serviceorder.ServiceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tomasz on 22.06.2017.
 */

public class JsonParseRequest {


    private JsonObjectRequest jsonObjectRequest;
    private String tempJson;

    public void volleyConnector(_Model model, RequestQueue queue, String requestUrl) {
        final JSONObject jsonObject;
        JsonParser jsonParser = new JsonParser();
        jsonObject = jsonParser.toJson(model);

        jsonObjectRequest = new JsonObjectRequest(requestUrl, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Response: " + response.toString());
                System.out.println("Object: " + jsonObject.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());
                System.out.println("Object: " + jsonObject.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        queue.add(jsonObjectRequest);
    }

    public String jsonFromServerRequest(final VolleyCallback callback, String requestUrl, RequestQueue queue) {

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                tempJson = response.toString();

                callback.onSuccess(response);
                System.out.println("Response: " + response.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());

            }
        });
        queue.add(jsonObjectRequest);

        return tempJson;

    }

    public interface VolleyCallback {
        void onSuccess(JSONObject result);
    }

    public String useData(RequestQueue queue, String url, final Activity activity) {
        jsonFromServerRequest(new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                tempJson = result.toString();
                if(activity instanceof ServiceActivity) {
                    ServiceActivity serviceActivity = new ServiceActivity();
                    serviceActivity.assignFields(tempJson, activity);
                }
//                switch (activity) {
//                    case ServiceActivity:
//                        ServiceActivity serviceActivity = new ServiceActivity();
//                        serviceActivity.assignFields(tempJson);
//                        break;
//                }
                System.out.println(tempJson + " tempJson z useDAta");
            }
        }, url, queue);
        return tempJson;
    }
}

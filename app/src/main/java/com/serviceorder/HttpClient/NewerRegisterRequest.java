package com.serviceorder.HttpClient;


import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.serviceorder.Model.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tomasz on 07.06.2017.
 */

public class NewerRegisterRequest {

    public void volleyConnector(Person person, RequestQueue queue) {
        final String REGISTER_REQUEST_URL = "http://10.0.2.2:8080/register";
        final JSONObject userJson = new JSONObject();


        try {

            userJson.put("username", person.getUsername());
            userJson.put("password"  , person.getPassword());
            userJson.put("email" , person.getEmail());


        } catch(JSONException e) {

            Log.v("JSON ERROR: ", e.toString());
        }



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(REGISTER_REQUEST_URL, userJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Response: " + response.toString());
                System.out.println("Object: " + userJson.toString());

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());
                System.out.println("Object: " +  userJson.toString());

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        queue.add(jsonObjectRequest);
    }

}

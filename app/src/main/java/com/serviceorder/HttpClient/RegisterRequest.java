package com.serviceorder.HttpClient;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.serviceorder.Model.Person;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tomasz on 06.06.2017.
 */

public class RegisterRequest extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "http://10.0.2.2:8010/register";
    private Map<String, String> params;


    public RegisterRequest(Person person, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                    }
                }
        );
        params = new HashMap<>();
        params.put("username", person.getUsername());
        params.put("password", person.getPassword());
        params.put("firstname", "adam");
        params.put("lastname", "adam");
        params.put("email", person.getEmail());


    }

    @Override
    public Map<String, String> getParams() {
        System.out.println(params.toString());
        return params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        //headers.put("key", "Value");
        return headers;
    }

}



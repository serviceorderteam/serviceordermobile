package com.serviceorder.HttpClient;

import android.app.DownloadManager;
import android.app.admin.SystemUpdatePolicy;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.serviceorder.Model.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tomasz on 05.06.2017.
 */

public class LoginRequest {

    public void volleyConnector(Person person, RequestQueue queue) {
        final String REGISTER_REQUEST_URL = "http://10.0.2.2:8020/auth";
        final JSONObject jsonObject = new JSONObject();


        try {

            jsonObject.put("username", person.getUsername());
            jsonObject.put("password"  , person.getPassword());


        } catch(JSONException e) {

            Log.v("JSON ERROR: ", e.toString());
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(REGISTER_REQUEST_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Response: " + response.toString());
                System.out.println("Object: " + jsonObject.toString());

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());
                System.out.println("Object: " +  jsonObject.toString());

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };
        queue.add(jsonObjectRequest);
    }

}

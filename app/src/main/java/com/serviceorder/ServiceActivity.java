package com.serviceorder;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.serviceorder.HttpClient.JsonParseRequest;
import com.serviceorder.Model.Service;


public class ServiceActivity extends AppCompatActivity {

    private String tempJson;
    private JsonParseRequest jsonParseRequest;
    private String url;
    private RequestQueue queue;
    private Activity activity = this;

    //@Bind(R.id.text_service_name) TextView serviceNameText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);


        String id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id= null;
            } else {
                id= String.valueOf(extras.getLong("id"));
            }
        } else {
            id= (String) savedInstanceState.getSerializable("id");
        }


        url = "http://192.168.0.19:8050/search/getService/"+id;
        jsonParseRequest = new JsonParseRequest();
        runAsync();

    }


    public void assignFields(String response, Activity activity) {
        Gson gson = new Gson();
        Service service = gson.fromJson(response, Service.class);

        View view = activity.findViewById(android.R.id.content);

        TextView serviceCategoryText;
        TextView serviceNameText;
        serviceNameText = (TextView) view.findViewById(R.id.text_service_name);
        serviceCategoryText = (TextView) view.findViewById(R.id.text_service_category);

        serviceCategoryText.setText(service.getCategory().getName());
        serviceNameText.setText(service.getServiceName());


    }


    public void runAsync()
    {
        queue = Volley.newRequestQueue(this);
        new readServiceFromDatabase().execute(queue, url);
    }

    public class readServiceFromDatabase extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {

            jsonParseRequest.useData(queue, url, activity);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }
}

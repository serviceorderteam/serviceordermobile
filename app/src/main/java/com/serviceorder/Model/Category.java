package com.serviceorder.Model;

import java.util.List;

/**
 * Created by Tomasz on 23.06.2017.
 */

public class Category {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    private List<Service> services;
}

package com.serviceorder.Model;

import android.media.Rating;

import java.util.List;

/**
 * Created by Tomasz on 21.06.2017.
 */

public class Service implements _Model{

    private String serviceName;

    public long getId() {
        return id;
    }

    private long id;



    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    private float rating;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    private Category category;


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}

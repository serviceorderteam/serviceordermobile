package com.serviceorder;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.serviceorder.Model.Service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment {


    ListView serviceList;
    SearchView searchView;
    JsonArrayRequest jsonArrayRequest;
    String tempJson = "";
    RequestQueue queue;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        serviceList = (ListView) view.findViewById(R.id.list_view);
        setHasOptionsMenu(true);
        serviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ServiceActivity.class);
                Service service;
                service = (Service) serviceList.getItemAtPosition(position);
                intent.putExtra("id", service.getId());
                startActivity(intent);
            }
        });
        queue = Volley.newRequestQueue(this.getActivity());
        final String urlRequest = "http://192.168.0.19:8050/search/getAll";
        runJsonFromServer(urlRequest);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.navigation, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) searchItem.getActionView();
        searchView.setIconified(true);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String urlRequests = "http://192.168.0.19:8050/search/getServicesContaining/" + newText;
                runJsonFromServer(urlRequests);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }



    public class readJsonFromServer extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            tempJson = useData(queue, getActivity(), url);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
        }
    }

    public class readLocalJson extends AsyncTask<String, String, List<Service>> {

        ArrayList<String> valueArray = new ArrayList<String>();


        @Override
        protected List<Service> doInBackground(String... strings) {

            JSONArray jsonArray = null;

            try {
                jsonArray = new JSONArray(tempJson);
            } catch (JSONException e) {

            }

            List<Service> serviceList = new ArrayList<>();
            Gson gson = new Gson();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonData = null;
                JSONObject jsonData1;
                String tempCategoryName;
                try {
                    jsonData = jsonArray.getJSONObject(i);
                    Service service = gson.fromJson(jsonData.toString(), Service.class);
//                    service.setServiceName(jsonData.getString("serviceName"));
//                    service.setRating((float) jsonData.getDouble("rating"));
//
//                    Category category = new Category();
//                    tempCategoryName = jsonData.getString("category");
//                    jsonData1 = new JSONObject(tempCategoryName);
//                    System.out.println(jsonData1.getString("name"));
//                    category.setName(jsonData1.getString("name"));
//                    service.setCategory(category);

                    serviceList.add(service);
                    //valueArray.add(jsonData.getString("serviceName"));
                    //categoryArray.add(jsonData.getString("name"));
                } catch (JSONException e) {

                }
            }

            return serviceList;
        }

        @Override
        protected void onPostExecute(List<Service> result) {
            super.onPostExecute(result);


            ServiceAdapter adapter = new ServiceAdapter(getActivity(), R.layout.service_list, result);
            serviceList.setAdapter(adapter);
        }
    }

    public void runAsync() {
        new readLocalJson().execute();
    }

    public void runJsonFromServer(String urlRequest) {

        new readJsonFromServer().execute(urlRequest);
    }


    public interface VolleyCallback {
        void onSuccess(JSONArray result);
    }

    public String useData(RequestQueue queue, final Activity activity, String url) {
        jsonFromServerRequest(new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                tempJson = result.toString();
                if (activity instanceof NavigationActivity) {
                    runAsync();
                }
            }
        }, url, queue);
        return tempJson;
    }

    public void jsonFromServerRequest(final VolleyCallback callback, String requestUrl, RequestQueue queue) {

        jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    callback.onSuccess(response);
                    System.out.println("Response: " + response.toString());

                } catch (Exception e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.toString());

            }
        });
        queue.add(jsonArrayRequest);

    }


    public class ServiceAdapter extends ArrayAdapter {

        private List<Service> serviceModelList;
        private int resource;
        private LayoutInflater inflater;

        public ServiceAdapter(Context context, int resource, List<Service> objects) {
            super(context, resource, objects);

            serviceModelList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(resource, null);
            }

            ImageView logo;
            TextView serviceName;
            TextView category;
            RatingBar ratingBar;

            logo = (ImageView) convertView.findViewById(R.id.logo);
            serviceName = (TextView) convertView.findViewById(R.id.label);
            category = (TextView) convertView.findViewById(R.id.text_category);
            ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);

            serviceName.setText(serviceModelList.get(position).getServiceName());
            category.setText(serviceModelList.get(position).getCategory().getName());
            ratingBar.setRating(serviceModelList.get(position).getRating());


            return convertView;
        }
    }

}
